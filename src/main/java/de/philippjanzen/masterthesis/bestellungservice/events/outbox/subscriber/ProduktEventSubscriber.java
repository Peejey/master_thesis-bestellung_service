package de.philippjanzen.masterthesis.bestellungservice.events.outbox.subscriber;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.LagerbestandReduzierenErrorEvent;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer.ProduktEventConsumer;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import io.reactivex.Observable;
import io.reactivex.Observer;

public class ProduktEventSubscriber implements Subscriber<DomainEventEnvelope> {

	private ObjectMapper objectMapper;
	private ProduktEventConsumer produktEventConsumer;

	public ProduktEventSubscriber(ProduktEventConsumer produktEventConsumer, ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		this.produktEventConsumer = produktEventConsumer;
	}

	@Override
	public void onSubscribe(Subscription subscription) {
		// TODO Auto-generated method stub

	}

	public void onNext(DomainEventEnvelope item) {
		if (item.getAggregateType().equals("de.philippjanzen.masterthesis.produktservice.produkt.Produkt")) {
			if (item.getEventType().equals(
					"de.philippjanzen.masterthesis.produktservice.produkt.events.LagerbestandReduziertErrorEvent")) {
				try {
					LagerbestandReduzierenErrorEvent errorEvent = objectMapper.readValue(item.getEvent(),
							LagerbestandReduzierenErrorEvent.class);
					produktEventConsumer.accept(errorEvent, item.getAggregateId(), item.getCorrelationId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub

	}

}
