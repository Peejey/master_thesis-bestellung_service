package de.philippjanzen.masterthesis.bestellungservice.events.domainevents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BestellungAbgebrochenEvent extends BestellungEvent {

	@JsonProperty
	private String x;
	@JsonProperty
	private String bla;

	protected BestellungAbgebrochenEvent() {
	}

	public BestellungAbgebrochenEvent(String bla, String x) {
		this.x = x;
		this.bla = bla;
	}

	public String getBla() {
		return bla;
	}

	public void setBla(String bla) {
		this.bla = bla;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

}
