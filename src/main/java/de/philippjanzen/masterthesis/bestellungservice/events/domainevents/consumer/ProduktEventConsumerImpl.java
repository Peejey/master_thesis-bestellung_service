package de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer;

import org.springframework.beans.factory.annotation.Autowired;

import de.philippjanzen.masterthesis.bestellungservice.bestellung.BestellungService;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.LagerbestandReduzierenErrorEvent;

public class ProduktEventConsumerImpl implements ProduktEventConsumer {

	@Autowired
	private BestellungService bestellungService;

	public ProduktEventConsumerImpl() {
	}

	public void accept(LagerbestandReduzierenErrorEvent lagerbestandReduzierenErrorEvent, String aggregateId,
			String correlationId) {
		bestellungService.brecheBestellungAb(correlationId);
	}

}
