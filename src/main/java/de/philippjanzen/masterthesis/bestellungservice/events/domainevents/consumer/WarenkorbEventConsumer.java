package de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer;

import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.WarenkorbLeerenErrorEvent;

public interface WarenkorbEventConsumer {

	public void accept(WarenkorbLeerenErrorEvent warenkorbLeerenErrorEvent, String aggregateId, String correlationId);

}
