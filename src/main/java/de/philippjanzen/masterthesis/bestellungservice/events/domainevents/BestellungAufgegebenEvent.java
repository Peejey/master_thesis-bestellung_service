package de.philippjanzen.masterthesis.bestellungservice.events.domainevents;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.philippjanzen.masterthesis.bestellungservice.entities.Artikel;
import de.philippjanzen.masterthesis.bestellungservice.entities.Zahlungsinformation;

public class BestellungAufgegebenEvent extends BestellungEvent {

	@JsonProperty
	private List<Artikel> artikel;
	@JsonProperty
	private double gesamtsumme;
	@JsonProperty
	private String bestellDatum;
	@JsonProperty
	private String warenkorbId;
	@JsonProperty
	private Zahlungsinformation zahlungsinformation;

	protected BestellungAufgegebenEvent() {
	}

	public BestellungAufgegebenEvent(List<Artikel> artikel, double gesamtsumme, String bestellDatum, String warenkorbId,
			Zahlungsinformation zahlungsinformation) {
		this.artikel = artikel;
		this.gesamtsumme = gesamtsumme;
		this.bestellDatum = bestellDatum;
		this.warenkorbId = warenkorbId;
		this.zahlungsinformation = zahlungsinformation;

	}

}
