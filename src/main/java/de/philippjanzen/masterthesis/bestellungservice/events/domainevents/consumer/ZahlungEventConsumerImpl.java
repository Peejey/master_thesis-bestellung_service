package de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer;

import org.springframework.beans.factory.annotation.Autowired;

import de.philippjanzen.masterthesis.bestellungservice.bestellung.BestellungService;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.ZahlungGetaetigtErrorEvent;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.ZahlungGetaetigtEvent;

public class ZahlungEventConsumerImpl implements ZahlungEventConsumer {

	@Autowired
	private BestellungService bestellungService;

	public ZahlungEventConsumerImpl() {
	}

	@Override
	public void accept(ZahlungGetaetigtEvent zahlungGetaetigtEvent, String aggregateId, String correlationId) {
		bestellungService.genehmigeBestellung(correlationId, aggregateId);
	}

	@Override
	public void accept(ZahlungGetaetigtErrorEvent zahlungGetaetigtErrorEvent, String aggregateId,
			String correlationId) {
		bestellungService.brecheBestellungAb(correlationId);
	}
}
