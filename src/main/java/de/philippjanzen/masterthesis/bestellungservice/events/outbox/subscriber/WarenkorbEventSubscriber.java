package de.philippjanzen.masterthesis.bestellungservice.events.outbox.subscriber;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.WarenkorbLeerenErrorEvent;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer.WarenkorbEventConsumer;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;

public class WarenkorbEventSubscriber implements Subscriber<DomainEventEnvelope> {
	private ObjectMapper objectMapper;
	private WarenkorbEventConsumer warenkorbEventConsumer;

	public WarenkorbEventSubscriber(WarenkorbEventConsumer warenkorbEventConsumer, ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		this.warenkorbEventConsumer = warenkorbEventConsumer;
	}

	@Override
	public void onSubscribe(Subscription subscription) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNext(DomainEventEnvelope item) {
		if (item.getAggregateType().equals("de.philippjanzen.masterthesis.warenkorbservice.warenkorb.Warenkorb")) {
			if (item.getEventType()
					.equals("de.philippjanzen.masterthesis.warenkorbservice.events.LagerbestandReduziertErrorEvent")) {
				try {
					WarenkorbLeerenErrorEvent errorEvent = objectMapper.readValue(item.getEvent(),
							WarenkorbLeerenErrorEvent.class);
					warenkorbEventConsumer.accept(errorEvent, item.getAggregateId(), item.getCorrelationId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub

	}
}
