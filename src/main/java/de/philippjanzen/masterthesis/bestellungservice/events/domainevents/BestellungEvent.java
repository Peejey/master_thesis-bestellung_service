package de.philippjanzen.masterthesis.bestellungservice.events.domainevents;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEvent;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;

public abstract class BestellungEvent extends DomainEvent {

	@Override
	public String getChannel() {
		return EventsConnectionConfig.BESTELLUNG_SERVICE_CHANNEL;
	}
}
