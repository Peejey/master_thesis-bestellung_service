package de.philippjanzen.masterthesis.bestellungservice.events.outbox.subscriber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Flow.Subscriber;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.bestellungservice.bestellung.BestellungService;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer.ProduktEventConsumerImpl;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer.WarenkorbEventConsumerImpl;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer.ZahlungEventConsumerImpl;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import io.reactivex.Observable;

@Component
public class DomainEventSubscribeManager {

	private ChannelManager<DomainEventEnvelope> channelManager;
	private ObjectMapper objectMapper;

	public DomainEventSubscribeManager(ChannelManager<DomainEventEnvelope> channelManager, ObjectMapper objectMapper,
			BestellungService bestellungService) {
		this.channelManager = channelManager;
		this.objectMapper = objectMapper;
		subscribeToAllChannels();
	}

	private void subscribeToAllChannels() {
//		ZahlungEventSubscriber zahlungEventSubscriber = new ZahlungEventSubscriber(new ZahlungEventConsumerImpl(),
//				objectMapper);
//		ProduktEventSubscriber produktEventSubscriber = new ProduktEventSubscriber(new ProduktEventConsumerImpl(),
//				objectMapper);
//		WarenkorbEventSubscriber warenkorbEventSubscriber = new WarenkorbEventSubscriber(
//				new WarenkorbEventConsumerImpl(), objectMapper);
//
//		Aggregation<DomainEventEnvelope> aggregation = new DomainEventEnvelopeAggregation(Consumer);
//
//		aggregation.add(zahlungEventSubscriber);
//		aggregation.add(produktEventSubscriber);
//		aggregation.add(warenkorbEventSubscriber);

		channelManager.subscribe(new ZahlungEventSubscriber(new ZahlungEventConsumerImpl(), objectMapper),
				Collections.singleton(EventsConnectionConfig.ZAHLUNG_SERVICE_CHANNEL));
		channelManager.subscribe(new ProduktEventSubscriber(new ProduktEventConsumerImpl(), objectMapper),
				Collections.singleton(EventsConnectionConfig.PRODUKT_SERVICE_CHANNEL));
		channelManager.subscribe(new WarenkorbEventSubscriber(new WarenkorbEventConsumerImpl(), objectMapper),
				Collections.singleton(EventsConnectionConfig.WARENKORB_SERVICE_CHANNEL));
	}

}
