package de.philippjanzen.masterthesis.bestellungservice.events.domainevents;

public class WarenkorbLeerenErrorEvent {

	private String bestellungId;

	public WarenkorbLeerenErrorEvent(String bestellungId) {
		this.bestellungId = bestellungId;
	}

	public String getBestellungId() {
		return bestellungId;
	}

}
