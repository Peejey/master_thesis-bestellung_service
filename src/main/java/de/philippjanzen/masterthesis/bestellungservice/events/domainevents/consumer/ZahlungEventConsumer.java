package de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer;

import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.ZahlungGetaetigtErrorEvent;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.ZahlungGetaetigtEvent;

public interface ZahlungEventConsumer {

	public void accept(ZahlungGetaetigtEvent zahlungGetaetigtEvent, String aggregateId, String correlationId);

	public void accept(ZahlungGetaetigtErrorEvent zahlungGetaetigtErrorEvent, String aggregateId, String correlationId);

}
