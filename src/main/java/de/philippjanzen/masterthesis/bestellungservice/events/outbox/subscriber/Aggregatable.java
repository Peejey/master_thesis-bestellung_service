//package de.philippjanzen.masterthesis.bestellungservice.events.outbox.subscriber;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public abstract class Aggregatable<T> {
//
//	private List<Aggregation<T>> aggregations = new ArrayList<>();
//
//	public void onAggregate(Aggregation<T> aggregation) {
//		aggregations.add(aggregation);
//	};
//
//	public final void onNext(T x) {
//		aggregations.forEach(aggregation -> {
//			aggregation.mark(this, x);
//		});
//		doOnNext(x);
//	}
//
//	public abstract void doOnNext(T x);
//
//}
