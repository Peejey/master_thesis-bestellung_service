//package de.philippjanzen.masterthesis.bestellungservice.events.outbox.subscriber;
//
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.UUID;
//import java.util.concurrent.Flow.Subscriber;
//import java.util.function.Consumer;
//
//import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
//
//public class DomainEventEnvelopeAggregation implements Aggregation<DomainEventEnvelope> {
//
//	private static final String AGGREGATE_TYPE = "aggregate_type";
//	private static final String EVENT_TYPE = "event_type";
//
//	private Map<Aggregatable<DomainEventEnvelope>, Boolean> flags = new HashMap<>();
//	private Map<Aggregatable<DomainEventEnvelope>, DomainEventEnvelope> values = new HashMap<>();
//	private Map<Aggregatable<DomainEventEnvelope>, Map<String, String>> metaDatas = new HashMap<>();
//	private String correlationId;
//	private Consumer<String> consumer;
//
//	public DomainEventEnvelopeAggregation(Consumer<String> consumer) {
//		this.consumer = consumer;
//	}
//
//	@Override
//	public void mark(Aggregatable<DomainEventEnvelope> aggregatable, DomainEventEnvelope value) {
//		flags.put(aggregatable, false);
//		values.put(aggregatable, value);
//		checkAggregation();
//	}
//
//	private void checkAggregation() {
//		boolean result = true;
//		for (Map.Entry<Aggregatable<DomainEventEnvelope>, Boolean> entry : flags.entrySet()) {
//			if (!entry.getValue()) {
//				result = false;
//			}
//		}
//		if (result == true) {
//			consumer.accept();
//		}
//	}
//
//	public void add(Aggregatable<DomainEventEnvelope> aggregatable, String aggregateType, String eventType) {
//		this.flags.put(aggregatable, false);
//		Map<String, String> map = new HashMap<>();
//		map.put(AGGREGATE_TYPE, aggregateType);
//		map.put(EVENT_TYPE, EVENT_TYPE);
//		metaDatas.put(aggregatable, map);
//
//		aggregatable.onAggregate(this);
//	}
//
//}
