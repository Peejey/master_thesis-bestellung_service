package de.philippjanzen.masterthesis.bestellungservice.events.domainevents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BestellungAbgebrochenAbgespecktEvent {

	@JsonProperty("bla")
	private String alb;

	protected BestellungAbgebrochenAbgespecktEvent() {
	}

	public BestellungAbgebrochenAbgespecktEvent(String alb) {
		this.alb = alb;
	}

	public String getAlb() {
		return alb;
	}

	public void setAlb(String alb) {
		this.alb = alb;
	}

}
