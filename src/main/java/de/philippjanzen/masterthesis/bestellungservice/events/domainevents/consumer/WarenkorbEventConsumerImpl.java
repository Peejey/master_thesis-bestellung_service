package de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer;

import org.springframework.beans.factory.annotation.Autowired;

import de.philippjanzen.masterthesis.bestellungservice.bestellung.BestellungService;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.WarenkorbLeerenErrorEvent;

public class WarenkorbEventConsumerImpl implements WarenkorbEventConsumer {

	@Autowired
	private BestellungService bestellungService;

	public WarenkorbEventConsumerImpl() {
	}

	public void accept(WarenkorbLeerenErrorEvent warenkorbLeerenErrorEvent, String aggregateId, String correlationId) {
		bestellungService.brecheBestellungAb(warenkorbLeerenErrorEvent.getBestellungId());
	}
}
