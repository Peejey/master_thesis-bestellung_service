package de.philippjanzen.masterthesis.bestellungservice.events;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.BestellungEvent;
import de.philippjanzen.masterthesis.sharedservice.events.CustomChannelMapper;
import de.philippjanzen.masterthesis.sharedservice.events.CustomChannelMapperImpl;
import de.philippjanzen.masterthesis.sharedservice.events.CustomMessageProducer;
import de.philippjanzen.masterthesis.sharedservice.events.CustomMessageProducerImpl;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelopeRepository;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventPublisher;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventPublisherImpl;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;

@Configuration
public class EventsConfig {

	@Bean
	DomainEventPublisher<BestellungEvent> domainEventPublisher(
			DomainEventEnvelopeRepository domainEventEnvelopeRepository,
			ApplicationEventPublisher applicationEventPublisher) {
		return new DomainEventPublisherImpl<BestellungEvent>(domainEventEnvelopeRepository, objectMapper(),
				applicationEventPublisher);
	}

	@Bean
	ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objectMapper;
	}

	@Bean
	CustomMessageProducer<DomainEventEnvelope> customMessageProducer(RabbitTemplate rabbitTemplate,
			AmqpConnectionConfig rabbitConnectionConfig, CustomChannelMapper channelMapping) {
		return new CustomMessageProducerImpl<>(rabbitTemplate, rabbitConnectionConfig, channelMapping);
	}

	@Bean
	CustomChannelMapper customChannelMapper(AmqpConnectionConfig rabbitmqConnectionConfig) {
		return new CustomChannelMapperImpl(rabbitmqConnectionConfig);
	}

}
