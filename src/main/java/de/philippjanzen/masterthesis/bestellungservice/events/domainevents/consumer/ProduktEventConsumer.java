package de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer;

import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.LagerbestandReduzierenErrorEvent;

public interface ProduktEventConsumer {

	public void accept(LagerbestandReduzierenErrorEvent lagerbestandReduzierenErrorEvent, String aggregateId,
			String correlationId);

}
