package de.philippjanzen.masterthesis.bestellungservice.events.outbox.subscriber;

import java.io.IOException;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.ZahlungGetaetigtErrorEvent;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.ZahlungGetaetigtEvent;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.consumer.ZahlungEventConsumer;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;

public class ZahlungEventSubscriber implements Subscriber<DomainEventEnvelope> {

	private ObjectMapper objectMapper;
	private ZahlungEventConsumer zahlungEventConsumer;

	public ZahlungEventSubscriber(ZahlungEventConsumer zahlungEventConsumer, ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		this.zahlungEventConsumer = zahlungEventConsumer;
	}

	@Override
	public void onSubscribe(Subscription subscription) {
	}

	@Override
	public void onNext(DomainEventEnvelope item) {
		if (item.getAggregateType().equals("de.philippjanzen.masterthesis.zahlungservice.zahlung.Zahlung")) {
			if (item.getEventType()
					.equals("de.philippjanzen.masterthesis.zahlungservice.zahlung.events.ZahlungGetaetigtEvent")) {
				try {
					ZahlungGetaetigtEvent zahlungGetaetigtEvent = objectMapper.readValue(item.getEvent(),
							ZahlungGetaetigtEvent.class);
					zahlungEventConsumer.accept(zahlungGetaetigtEvent, item.getAggregateId(), item.getCorrelationId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (item.getEventType()
					.equals("de.philippjanzen.masterthesis.zahlungservice.zahlung.events.ZahlungGetaetigtErrorEvent")) {
				try {
					ZahlungGetaetigtErrorEvent zahlungGetaetigtErrorEvent = objectMapper.readValue(item.getEvent(),
							ZahlungGetaetigtErrorEvent.class);
					zahlungEventConsumer.accept(zahlungGetaetigtErrorEvent, item.getAggregateId(),
							item.getCorrelationId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub

	}

}
