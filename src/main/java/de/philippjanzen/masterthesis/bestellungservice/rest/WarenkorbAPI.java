package de.philippjanzen.masterthesis.bestellungservice.rest;

public interface WarenkorbAPI {
	public boolean leereWarenkorb(String warenkorbId);
}
