package de.philippjanzen.masterthesis.bestellungservice.rest;

import de.philippjanzen.masterthesis.bestellungservice.dtos.ZahlungValidationDTO;
import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;

public interface ZahlungAPI {
	public String taetigeZahlung(ZahlungsinformationDTO zahlungsinformation);

	public ZahlungValidationDTO storniereZahlung(String zahlungsReferenz);
}
