package de.philippjanzen.masterthesis.bestellungservice.rest;

import java.util.List;

import de.philippjanzen.masterthesis.bestellungservice.produkt.Produkt;
import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;

public interface ProduktAPI {

	public List<Produkt> leseProdukte();

	public boolean aktualisiereLagerbestand(String produktId, int bestand);

	public Produkt leseProdukt(String produktId);

	public boolean aktualisiereLagerbestand(List<LagerbestandsAktualisierungDTO> lagerbestaende);
}
