package de.philippjanzen.masterthesis.bestellungservice.rest;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import de.philippjanzen.masterthesis.bestellungservice.dtos.ZahlungValidationDTO;
import de.philippjanzen.masterthesis.bestellungservice.produkt.Produkt;
import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;

public class RestCommunication implements ProduktAPI, WarenkorbAPI, ZahlungAPI {

	RestTemplate restTemplate = new RestTemplate();
	String baseUrlProduktservice = "http://localhost:8010";
	String baseUrlWarenkorbservice = "http://localhost:8011";
	String baseUrlZahlungservice = "http://localhost:8013";
	HttpHeaders headers = new HttpHeaders();

	public RestCommunication() {
		headers.setContentType(MediaType.APPLICATION_JSON);
	}

	public List<Produkt> leseProdukte() {

		ResponseEntity<List<Produkt>> response = restTemplate.exchange(baseUrlProduktservice + "/produkte",
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Produkt>>() {
				});
		return response.getBody();
	}

	@Override
	public boolean aktualisiereLagerbestand(String produktId, int bestand) {
		ResponseEntity<Produkt> response = restTemplate.exchange(
				baseUrlProduktservice + "/produkte/" + produktId + "/lagerbestand", HttpMethod.POST,
				new HttpEntity<>(bestand, headers), new ParameterizedTypeReference<Produkt>() {
				});
		return response.getStatusCode().is2xxSuccessful();
	}

	public Produkt leseProdukt(String produktId) {
		ResponseEntity<Produkt> response = restTemplate.exchange(baseUrlProduktservice + "/produkte/" + produktId,
				HttpMethod.GET, null, new ParameterizedTypeReference<Produkt>() {
				});
		return response.getBody();
	}

	public boolean leereWarenkorb(String warenkorbId) {
		ResponseEntity<String> response = restTemplate.exchange(baseUrlWarenkorbservice + "/warenkorb/" + warenkorbId,
				HttpMethod.DELETE, null, new ParameterizedTypeReference<String>() {
				});
		return response.getStatusCode().is2xxSuccessful();
	}

	public String taetigeZahlung(ZahlungsinformationDTO zahlungsinformation) {
		ResponseEntity<String> response = restTemplate.exchange(baseUrlZahlungservice + "/zahlung", HttpMethod.POST,
				new HttpEntity<>(zahlungsinformation, headers), new ParameterizedTypeReference<String>() {
				});
		return response.getBody();
	}

	@Override
	public ZahlungValidationDTO storniereZahlung(String zahlungsReferenz) {
		ResponseEntity<ZahlungValidationDTO> response = restTemplate.exchange(
				baseUrlZahlungservice + "/zahlung/" + zahlungsReferenz, HttpMethod.POST,
				new HttpEntity<>(zahlungsReferenz, headers), new ParameterizedTypeReference<ZahlungValidationDTO>() {
				});
		return response.getBody();
	}

	public boolean aktualisiereLagerbestand(List<LagerbestandsAktualisierungDTO> lagerbestaende) {
		ResponseEntity<List<Produkt>> response = restTemplate.exchange(
				baseUrlProduktservice + "/produkte/lagerbestandsaktualisierung", HttpMethod.POST,
				new HttpEntity<>(lagerbestaende, headers), new ParameterizedTypeReference<List<Produkt>>() {
				});
		return response.getStatusCode().is2xxSuccessful();

	}

}
