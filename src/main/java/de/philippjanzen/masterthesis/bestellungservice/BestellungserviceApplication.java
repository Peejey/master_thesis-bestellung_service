package de.philippjanzen.masterthesis.bestellungservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = { "de.philippjanzen.masterthesis.bestellungservice",
		"de.philippjanzen.masterthesis.sharedservice.events" })
@ComponentScan(basePackages = { "de.philippjanzen.masterthesis.bestellungservice",
		"de.philippjanzen.masterthesis.sharedservice.events" })
@EntityScan(basePackages = { "de.philippjanzen.masterthesis.bestellungservice",
		"de.philippjanzen.masterthesis.sharedservice.events" })
public class BestellungserviceApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(BestellungserviceApplication.class, args);

	}

}
