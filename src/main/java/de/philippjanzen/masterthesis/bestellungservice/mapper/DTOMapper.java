package de.philippjanzen.masterthesis.bestellungservice.mapper;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.bestellungservice.bestellung.ArtikelInformationen;
import de.philippjanzen.masterthesis.bestellungservice.bestellung.BestellStatus;
import de.philippjanzen.masterthesis.bestellungservice.dtos.BestellungAnfrageDTO;
import de.philippjanzen.masterthesis.bestellungservice.dtos.BestellungInfosDTO;
import de.philippjanzen.masterthesis.bestellungservice.entities.Artikel;
import de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung;
import de.philippjanzen.masterthesis.bestellungservice.entities.Zahlungsinformation;
import de.philippjanzen.masterthesis.bestellungservice.produkt.Produkt;
import de.philippjanzen.masterthesis.bestellungservice.produkt.ProduktService;
import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;

@Service
public class DTOMapper {
	private ArtikelInformationen artikelInformationen;

	public DTOMapper(ArtikelInformationen artikelInformationen) {
		this.artikelInformationen = artikelInformationen;
	}

	public Bestellung mappeBestellungAnfrage(BestellungAnfrageDTO bestellungAnfrageDTO) {
		Bestellung bestellung = new Bestellung();
		bestellung.setWarenkorbId(bestellungAnfrageDTO.getWarenkorbId());
		bestellung.setBestellStatus(BestellStatus.ANGEFRAGT);

		bestellungAnfrageDTO.getArtikel().stream().forEach(artikel -> {
			artikelInformationen.gleicheInformationenAb(artikel);
			bestellung.fuegeArtikelHinzu(artikel);
		});

		return bestellung;
	}

	public void mappeBestellungInfos(Bestellung targetBestellung, BestellungInfosDTO bestellungInfosDTO) {
		if (bestellungInfosDTO.getAnschrift() == null || !bestellungInfosDTO.getAnschrift().isValid()) {
			throw new IllegalArgumentException("Es fehlen notwendige Informationen über die Anschrift!");
		} else if (bestellungInfosDTO.getZahlungsinformation() == null
				|| !bestellungInfosDTO.getZahlungsinformation().isValid()) {
			throw new IllegalArgumentException("Es fehlen die notwendigen Zahlungsinformationen!");
		}

		targetBestellung.setZahlungsinformation(bestellungInfosDTO.getZahlungsinformation());
		targetBestellung.setAnschrift(bestellungInfosDTO.getAnschrift());
	}

	public ZahlungsinformationDTO mappeZahlungsinformation(Zahlungsinformation zahlungsinformation,
			double zahlungssumme, String referenzId) {
		return new ZahlungsinformationDTO(zahlungsinformation.getIban(), zahlungsinformation.getBic(),
				zahlungsinformation.getKontoinhaber(), zahlungsinformation.getBank(), referenzId, zahlungssumme);
	}

	private LagerbestandsAktualisierungDTO mappeLagerbestandsAktualisierung(Artikel artikel, boolean reduzieren) {
		LagerbestandsAktualisierungDTO lagerbestandsAktualisierungDTO = new LagerbestandsAktualisierungDTO();
		lagerbestandsAktualisierungDTO.setMenge(artikel.getMenge() * (reduzieren ? -1 : 1));
		lagerbestandsAktualisierungDTO.setProduktId(artikel.getReferenzId());
		return lagerbestandsAktualisierungDTO;
	}

	public List<LagerbestandsAktualisierungDTO> mappeLagerbestandsAktualisierung(Bestellung bestellung,
			boolean reduzieren) {
		List<LagerbestandsAktualisierungDTO> lagerbestaende = new LinkedList<>();
		bestellung.getArtikel().forEach(artikel -> {
			lagerbestaende.add(mappeLagerbestandsAktualisierung(artikel, reduzieren));
		});
		return lagerbestaende;
	}

}
