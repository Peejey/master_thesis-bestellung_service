package de.philippjanzen.masterthesis.bestellungservice.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.bestellungservice.entities.Artikel;
import de.philippjanzen.masterthesis.bestellungservice.produkt.Produkt;
import de.philippjanzen.masterthesis.bestellungservice.produkt.ProduktService;

@Service
public class ProduktMapper {

	@Autowired
	private ProduktService produktService;

	public void gleicheArtikelAb(Artikel artikel) {
		Produkt produkt = produktService.findeProduktAnhandArtikel(artikel);
		if (produkt.getLagerbestand() >= artikel.getMenge()) {
			gleicheArtikelMitProduktAb(artikel, produkt);
		} else {
			throw new IllegalArgumentException(
					"Der Lagerbestand des Produkts '" + produkt.getName() + "' von '" + produkt.getLagerbestand()
							+ "' Stück reicht für die gewünschte Menge von '" + artikel.getMenge() + "' nicht aus!");
		}
	}

	private void gleicheArtikelMitProduktAb(Artikel targetArtikel, Produkt produkt) {
		targetArtikel.setName(produkt.getName());
		targetArtikel.setEinzelpreis(produkt.getPreis());
	}
}
