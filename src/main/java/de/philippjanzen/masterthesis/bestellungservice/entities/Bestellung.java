package de.philippjanzen.masterthesis.bestellungservice.entities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonFormat;

import de.philippjanzen.masterthesis.bestellungservice.bestellung.BestellStatus;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.BestellungAufgegebenEvent;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.BestellungEvent;
import de.philippjanzen.masterthesis.bestellungservice.utils.CalcUtils;
import de.philippjanzen.masterthesis.sharedservice.events.DomainAggregate;

@Entity
public class Bestellung extends DomainAggregate {

	@Id
	private String id;
	private String timestamp;
	private String bestellDatum;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "bestellung_id")
	private List<Artikel> artikel = new ArrayList<>();
	private double gesamtsumme;
	private double versandkosten;
	private double mwst;
	@OneToOne(cascade = CascadeType.ALL)
	private Zahlungsinformation zahlungsinformation;
	private String zahlungsReferenz;
	@OneToOne(cascade = CascadeType.ALL)
	private Anschrift anschrift;
	private String warenkorbId;
	private static final double MIN_VERSANDKOSTEN = 4.50;
	private double artikelSumme;
	@Enumerated
	private BestellStatus bestellStatus;

	public Bestellung() {
		anschrift = new Anschrift();
		zahlungsinformation = new Zahlungsinformation();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setDatum(LocalDateTime timestamp) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		this.timestamp = timestamp.format(formatter).toString();
	}

	public List<Artikel> getArtikel() {
		return artikel;
	}

	public void setArtikel(List<Artikel> artikel) {
		this.artikel = artikel;
	}

	public double getGesamtsumme() {
		return gesamtsumme;
	}

	public void setGesamtsumme(double gesamtsumme) {
		this.gesamtsumme = gesamtsumme;
	}

	public double getVersandkosten() {
		return versandkosten;
	}

	public void setVersandkosten(double versandkosten) {
		this.versandkosten = versandkosten;
	}

	public double getMwst() {
		return mwst;
	}

	public void setMwst(double mwst) {
		this.mwst = mwst;
	}

	public Zahlungsinformation getZahlungsinformation() {
		return zahlungsinformation;
	}

	public void setZahlungsinformation(Zahlungsinformation zahlungsinformation) {
		this.zahlungsinformation = zahlungsinformation;
	}

	public String getZahlungsReferenz() {
		return zahlungsReferenz;
	}

	public void setZahlungsReferenz(String zahlungsReferenz) {
		this.zahlungsReferenz = zahlungsReferenz;
	}

	public Anschrift getAnschrift() {
		return anschrift;
	}

	public void setAnschrift(Anschrift anschrift) {
		this.anschrift = anschrift;
	}

	public String getWarenkorbId() {
		return warenkorbId;
	}

	public void setWarenkorbId(String warenkorbId) {
		this.warenkorbId = warenkorbId;
	}

	public double getArtikelSumme() {
		return artikelSumme;
	}

	public void setArtikelSumme(double artikelSumme) {
		this.artikelSumme = artikelSumme;
	}

	public BestellStatus getBestellStatus() {
		return bestellStatus;
	}

	public void setBestellStatus(BestellStatus bestellStatus) {
		this.bestellStatus = bestellStatus;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		this.timestamp = LocalDateTime.now().format(formatter).toString();
		if (bestellStatus == BestellStatus.WIRD_VERARBEITET) {
			bestellDatum = timestamp;
		}
	}

	public String getBestellDatum() {
		return bestellDatum;
	}

	public void bestellen() {
		setBestellStatus(BestellStatus.WIRD_VERARBEITET);
	}

	public BestellungEvent bestellenWithEvents() {
		setBestellStatus(BestellStatus.WIRD_VERARBEITET);
		return new BestellungAufgegebenEvent(artikel, gesamtsumme, bestellDatum.toString(), warenkorbId,
				zahlungsinformation);
	}

	public void stornieren() {
		setBestellStatus(BestellStatus.STORNIERT);
	}

	public void fuegeArtikelHinzu(Artikel artikel) {
		this.artikel.add(artikel);
		artikelSumme = CalcUtils.round(artikelSumme + artikel.getGesamtpreis());
		berechneVersandkosten();
		berechneMwst();
		berechneGesamtsumme();
	}

	private void berechneMwst() {
		mwst = CalcUtils.round(gesamtsumme * 0.19);
	}

	private void berechneVersandkosten() {
		double basis = CalcUtils.round(artikelSumme / 100.0);
		if (basis <= MIN_VERSANDKOSTEN) {
			versandkosten = MIN_VERSANDKOSTEN;
		} else {
			versandkosten = basis;
		}
	}

	private void berechneGesamtsumme() {
		gesamtsumme = CalcUtils.round(artikelSumme + versandkosten + mwst);
	}

	@PrePersist
	private void generateId() {
		if (id == null || id.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

}