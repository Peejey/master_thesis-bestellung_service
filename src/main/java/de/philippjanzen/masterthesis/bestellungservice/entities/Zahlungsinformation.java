package de.philippjanzen.masterthesis.bestellungservice.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;

@Entity
public class Zahlungsinformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 889642635927098061L;
	@Id
	private String id;
	private String iban;
	private String bic;
	private String kontoinhaber;
	private String bank;

//	@OneToOne
//	@PrimaryKeyJoinColumn
//	private Bestellung bestellung;

	public Zahlungsinformation() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getKontoinhaber() {
		return kontoinhaber;
	}

	public void setKontoinhaber(String kontoinhaber) {
		this.kontoinhaber = kontoinhaber;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public boolean isValid() {
		return !((iban == null || iban.isEmpty()) || (bic == null || bic.isEmpty())
				|| (kontoinhaber == null || kontoinhaber.isEmpty()) || (bank == null || bank.isEmpty()));
	}

	@PrePersist
	private void generateId() {
		if (id == null || id.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

}
