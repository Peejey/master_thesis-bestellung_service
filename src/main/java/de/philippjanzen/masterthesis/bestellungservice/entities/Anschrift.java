package de.philippjanzen.masterthesis.bestellungservice.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;

@Entity
public class Anschrift implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5624697443618584700L;
	@Id
	private String id;
	private String vorname;
	private String nachname;
	private String strasse;
	private String hausnummer;
	private String plz;
	private String ort;

	public Anschrift() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getHausnummer() {
		return hausnummer;
	}

	public void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public boolean isValid() {
		return !((vorname == null || vorname.isEmpty()) || (nachname == null || nachname.isEmpty())
				|| (strasse == null || strasse.isEmpty()) || (hausnummer == null || hausnummer.isEmpty())
				|| (plz == null || plz.isEmpty()) || (ort == null || ort.isEmpty()));
	}

	@PrePersist
	private void generateId() {
		if (id == null || id.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

}
