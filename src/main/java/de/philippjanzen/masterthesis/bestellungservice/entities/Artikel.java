package de.philippjanzen.masterthesis.bestellungservice.entities;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;

@Entity
public class Artikel {

	@Id
	private String id;
	private String name;
	private double einzelpreis;
	private int menge;
	private double gesamtpreis;
	private String referenzId;

	public Artikel() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getEinzelpreis() {
		return einzelpreis;
	}

	public void setEinzelpreis(double einzelpreis) {
		this.einzelpreis = einzelpreis;
		gesamtpreis = einzelpreis * menge;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
		gesamtpreis = menge * einzelpreis;
	}

	public double getGesamtpreis() {
		return gesamtpreis;
	}

	public void setGesamtpreis(double gesamtpreis) {
		this.gesamtpreis = gesamtpreis;
	}

	public String getReferenzId() {
		return referenzId;
	}

	public void setReferenzId(String referenzId) {
		this.referenzId = referenzId;
	}

	@PrePersist
	private void generateId() {
		if (id == null || id.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

}
