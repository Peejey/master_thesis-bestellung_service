package de.philippjanzen.masterthesis.bestellungservice.bestellung;

import org.springframework.data.repository.CrudRepository;

import de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung;

public interface BestellungRepository extends CrudRepository<Bestellung, String> {

}
