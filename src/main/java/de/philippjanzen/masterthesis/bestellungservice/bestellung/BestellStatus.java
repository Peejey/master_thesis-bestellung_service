package de.philippjanzen.masterthesis.bestellungservice.bestellung;

public enum BestellStatus {
	ANGEFRAGT, WIRD_VERARBEITET, BESTELLT, FEHLERHAFT_ABGEBROCHEN, STORNIERT;
}
