package de.philippjanzen.masterthesis.bestellungservice.bestellung;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.bestellungservice.dtos.BestellungAnfrageDTO;
import de.philippjanzen.masterthesis.bestellungservice.dtos.BestellungInfosDTO;
import de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung;
import de.philippjanzen.masterthesis.bestellungservice.events.domainevents.BestellungEvent;
import de.philippjanzen.masterthesis.bestellungservice.mapper.DTOMapper;
import de.philippjanzen.masterthesis.bestellungservice.rest.ProduktAPI;
import de.philippjanzen.masterthesis.bestellungservice.rest.WarenkorbAPI;
import de.philippjanzen.masterthesis.bestellungservice.rest.ZahlungAPI;
import de.philippjanzen.masterthesis.bestellungservice.saga.ErstelleBestellungSagaState;
import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventPublisher;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.tram.sagas.orchestration.SagaManager;

@Service
public class BestellungService {

	@Autowired
	private BestellungRepository bestellungRepository;
	@Autowired
	private DTOMapper dtoMapper;
	@Autowired
	private ProduktAPI produktApi;
	@Autowired
	private WarenkorbAPI warenkorbApi;
	@Autowired
	private ZahlungAPI zahlungApi;
	@Autowired
	private IdGenerator idGenerator;

//	Sagas Way:
	@Autowired
	private SagaManager<ErstelleBestellungSagaState> erstelleBestellungSagaManager;

	@Autowired
	private DomainEventPublisher<BestellungEvent> domainEventPublisher;

	public BestellungService() {

	}

	@Transactional
	public Bestellung erstelleBestellungsanfrage(BestellungAnfrageDTO bestellungAnfrageDTO) {
		Bestellung bestellung = dtoMapper.mappeBestellungAnfrage(bestellungAnfrageDTO);
		bestellungRepository.save(bestellung);
		return bestellung;
	}

	public List<Bestellung> leseAlleBestellungen() {
		List<Bestellung> bestellung = new ArrayList<>();
		bestellungRepository.findAll().forEach(b -> {
			if (b.getBestellStatus() != BestellStatus.ANGEFRAGT) {
				bestellung.add(b);
			}
		});
		return bestellung;
	}

	public Bestellung leseBestellung(String id) {
		return bestellungRepository.findById(id).get();
	}

	@Transactional
	public List<Bestellung> bestellen(String id, BestellungInfosDTO bestellungInfosDTO) {

		bestellenWithSaga(id, bestellungInfosDTO);
//		bestellenWithRest(id, bestellungInfosDTO);
//		bestellenWithEvents(id, bestellungInfosDTO);

		return leseAlleBestellungen();
	}

	private void bestellenWithEvents(String id, BestellungInfosDTO bestellungInfosDTO) {
		List<BestellungEvent> events = new ArrayList<>();
		Bestellung bestellung = bestellungRepository.findById(id).get();
		dtoMapper.mappeBestellungInfos(bestellung, bestellungInfosDTO);
		events.add(bestellung.bestellenWithEvents());
		bestellungRepository.save(bestellung);
		domainEventPublisher.publish(bestellung.getClass(), bestellung.getId(), null, events);
	}

	private void bestellenWithRest(String id, BestellungInfosDTO bestellungInfosDTO) {
		Bestellung bestellung = bestellungRepository.findById(id).get();
		dtoMapper.mappeBestellungInfos(bestellung, bestellungInfosDTO);
		bestellung.bestellen();
		List<LagerbestandsAktualisierungDTO> lagerbestaende = dtoMapper.mappeLagerbestandsAktualisierung(bestellung,
				true);
		ZahlungsinformationDTO zahlung = dtoMapper.mappeZahlungsinformation(bestellung.getZahlungsinformation(),
				bestellung.getGesamtsumme(), bestellung.getId());
		produktApi.aktualisiereLagerbestand(lagerbestaende);
		warenkorbApi.leereWarenkorb(bestellung.getWarenkorbId());
		bestellung.setZahlungsReferenz(zahlungApi.taetigeZahlung(zahlung));
	}

	private void bestellenWithSaga(String id, BestellungInfosDTO bestellungInfosDTO) {
		Bestellung bestellung = bestellungRepository.findById(id).get();
		dtoMapper.mappeBestellungInfos(bestellung, bestellungInfosDTO);
		bestellung.bestellen();
		bestellung = bestellungRepository.save(bestellung);
		List<LagerbestandsAktualisierungDTO> lagerbestaende = dtoMapper.mappeLagerbestandsAktualisierung(bestellung,
				true);
		ZahlungsinformationDTO zahlung = dtoMapper.mappeZahlungsinformation(bestellung.getZahlungsinformation(),
				bestellung.getGesamtsumme(), bestellung.getId());

		ErstelleBestellungSagaState erstelleBestellungSagaState = new ErstelleBestellungSagaState(
				idGenerator.genId().asString(), bestellung.getId(), bestellung.getWarenkorbId(), lagerbestaende,
				zahlung);
		erstelleBestellungSagaManager.create(erstelleBestellungSagaState, Bestellung.class, bestellung.getId());
	}

	@Transactional
	public List<Bestellung> storniereBestellung(String id) {
		Bestellung bestellung = bestellungRepository.findById(id).get();
		bestellung.setBestellStatus(BestellStatus.STORNIERT);
		bestellungRepository.save(bestellung);
		List<LagerbestandsAktualisierungDTO> lagerbestaende = dtoMapper.mappeLagerbestandsAktualisierung(bestellung,
				false);
		produktApi.aktualisiereLagerbestand(lagerbestaende);
		zahlungApi.storniereZahlung(bestellung.getZahlungsReferenz());
		return leseAlleBestellungen();
	}

	@Transactional
	public List<Bestellung> loescheBestellung(String id) {
		bestellungRepository.deleteById(id);
		List<Bestellung> bestellungen = new ArrayList<>();
		bestellungRepository.findAll().forEach(bestellungen::add);
		return bestellungen;
	}

	@Transactional
	public void genehmigeBestellung(String bestellungId, String zahlungsReferenz) {
		Bestellung bestellung = leseBestellung(bestellungId);
		bestellung.setZahlungsReferenz(zahlungsReferenz);
		bestellung.setBestellStatus(BestellStatus.BESTELLT);
		bestellungRepository.save(bestellung);
	}

	@Transactional
	public void brecheBestellungAb(String bestellungId) {
		Bestellung bestellung = leseBestellung(bestellungId);
		bestellung.setBestellStatus(BestellStatus.FEHLERHAFT_ABGEBROCHEN);
		bestellungRepository.save(bestellung);
	}

//	@Transactional
//	public void test() {
//		List<BestellungEvent> domainEvents = new LinkedList<>();
//		domainEvents.add(new BestellungAufgegebenEvent(null, 12345, LocalDateTime.now().toString(), "12345",
//				new Zahlungsinformation()));
//		domainEventPublisher.publish(Bestellung.class, UUID.randomUUID().toString(), domainEvents);
//	}

}
