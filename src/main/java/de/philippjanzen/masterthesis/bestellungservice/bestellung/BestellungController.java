package de.philippjanzen.masterthesis.bestellungservice.bestellung;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.philippjanzen.masterthesis.bestellungservice.dtos.BestellungAnfrageDTO;
import de.philippjanzen.masterthesis.bestellungservice.dtos.BestellungInfosDTO;
import de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung;

@RestController
public class BestellungController {

	private BestellungService bestellungService;

	public BestellungController(BestellungService bestellungService) {
		this.bestellungService = bestellungService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/bestellungen")
	public Bestellung erstelleBestellungsanfrage(@RequestBody BestellungAnfrageDTO bestellungAnfrageDTO) {
		return bestellungService.erstelleBestellungsanfrage(bestellungAnfrageDTO);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/bestellungen")
	public List<Bestellung> leseAlleBestellungen() {
		return bestellungService.leseAlleBestellungen();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/bestellungen/{id}")
	public Bestellung leseBestellung(@PathVariable String id) {
		return bestellungService.leseBestellung(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/bestellungen/{id}/bestellen")
	public List<Bestellung> bestellen(@PathVariable String id, @RequestBody BestellungInfosDTO bestellungInfosDTO) {
		return bestellungService.bestellen(id, bestellungInfosDTO);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/bestellungen/{id}/stornieren")
	public List<Bestellung> storniereBestellung(@PathVariable String id) {
		return bestellungService.storniereBestellung(id);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/bestellungen/{id}")
	public List<Bestellung> loescheBestellung(@PathVariable String id) {
		return bestellungService.loescheBestellung(id);
	}

//	@RequestMapping(method = RequestMethod.POST, value = "/test")
//	public void test() {
//		bestellungService.test();
//	}

}