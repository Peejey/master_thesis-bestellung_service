package de.philippjanzen.masterthesis.bestellungservice.bestellung;

import de.philippjanzen.masterthesis.bestellungservice.entities.Artikel;

public interface ArtikelInformationen {

	public void gleicheInformationenAb(Artikel artikel);
}
