package de.philippjanzen.masterthesis.bestellungservice.dtos;

import de.philippjanzen.masterthesis.bestellungservice.entities.Anschrift;
import de.philippjanzen.masterthesis.bestellungservice.entities.Zahlungsinformation;

public class BestellungInfosDTO {
	private Zahlungsinformation zahlungsinformation;
	private Anschrift anschrift;

	public BestellungInfosDTO() {
	}

	public Zahlungsinformation getZahlungsinformation() {
		return zahlungsinformation;
	}

	public void setZahlungsinformation(Zahlungsinformation zahlungsinformation) {
		this.zahlungsinformation = zahlungsinformation;
	}

	public Anschrift getAnschrift() {
		return anschrift;
	}

	public void setAnschrift(Anschrift anschrift) {
		this.anschrift = anschrift;
	}

}
