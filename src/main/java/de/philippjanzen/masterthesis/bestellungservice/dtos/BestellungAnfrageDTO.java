package de.philippjanzen.masterthesis.bestellungservice.dtos;

import java.util.List;

import de.philippjanzen.masterthesis.bestellungservice.entities.Artikel;

public class BestellungAnfrageDTO {

	private String warenkorbId;
	private List<Artikel> artikel;

	public BestellungAnfrageDTO() {
	}

	public String getWarenkorbId() {
		return warenkorbId;
	}

	public void setWarenkorbId(String warenkorbId) {
		this.warenkorbId = warenkorbId;
	}

	public List<Artikel> getArtikel() {
		return artikel;
	}

	public void setArtikel(List<Artikel> artikel) {
		this.artikel = artikel;
	}

}
