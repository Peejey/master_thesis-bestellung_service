package de.philippjanzen.masterthesis.bestellungservice.saga.repo;

import org.springframework.data.repository.CrudRepository;

import io.eventuate.tram.sagas.orchestration.SagaInstance;

public interface SagaRepository extends CrudRepository<SagaInstanceEntity, String> {

}
