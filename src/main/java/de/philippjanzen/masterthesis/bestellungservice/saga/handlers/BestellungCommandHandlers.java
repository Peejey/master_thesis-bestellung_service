package de.philippjanzen.masterthesis.bestellungservice.saga.handlers;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import de.philippjanzen.masterthesis.bestellungservice.bestellung.BestellungService;
import de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung;
import de.philippjanzen.masterthesis.bestellungservice.saga.commands.BrecheBestellungAb;
import de.philippjanzen.masterthesis.bestellungservice.saga.commands.GenehmigeBestellung;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.SagaCustomCommandHandlersBuilder;
import de.philippjanzen.masterthesis.sharedservice.saga.producers.SagaCustomReplyMessageBuilder;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.tram.commands.common.CommandReplyOutcome;
import io.eventuate.tram.commands.common.ReplyMessageHeaders;
import io.eventuate.tram.commands.common.Success;
import io.eventuate.tram.commands.consumer.CommandHandlers;
import io.eventuate.tram.commands.consumer.CommandMessage;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.sagas.common.SagaReplyHeaders;
import io.eventuate.tram.sagas.participant.SagaCommandHandlersBuilder;
import io.eventuate.tram.sagas.participant.SagaReplyMessage;
import io.eventuate.tram.sagas.participant.SagaReplyMessageBuilder;

public class BestellungCommandHandlers {

	@Autowired
	private IdGenerator idGenerator;

	@Autowired
	private BestellungService bestellungService;

	public BestellungCommandHandlers() {
	}

	public CommandHandlers commandHandlers() {
		return SagaCustomCommandHandlersBuilder.fromChannel(SagaConnectionConfig.BESTELLUNG_SERVICE_CHANNEL)
				.onMessage(GenehmigeBestellung.class, this::genehmigeBestellung)
				.onMessage(BrecheBestellungAb.class, this::brecheBestellungAb).build();
	}

	@Transactional
	public Message genehmigeBestellung(CommandMessage<GenehmigeBestellung> cm) {
		String bestellungId = cm.getCommand().getId();
		String zahlungsReferenz = cm.getCommand().getZahlungsreferenz();
		bestellungService.genehmigeBestellung(bestellungId, zahlungsReferenz);
		return new SagaCustomReplyMessageBuilder().withSuccess().withId(idGenerator.genId().asString()).build();
	}

	@Transactional
	public Message brecheBestellungAb(CommandMessage<BrecheBestellungAb> cm) {
		String bestellungId = cm.getCommand().getId();
		bestellungService.brecheBestellungAb(bestellungId);
		return new SagaCustomReplyMessageBuilder().withSuccess().withId(idGenerator.genId().asString()).build();
	}
}
