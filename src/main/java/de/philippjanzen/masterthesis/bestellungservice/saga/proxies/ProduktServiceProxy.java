package de.philippjanzen.masterthesis.bestellungservice.saga.proxies;

import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestand;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestandZurueck;
import io.eventuate.tram.commands.common.Success;
import io.eventuate.tram.sagas.simpledsl.CommandEndpoint;
import io.eventuate.tram.sagas.simpledsl.CommandEndpointBuilder;

public class ProduktServiceProxy {

	public final CommandEndpoint<AktualisiereLagerbestand> aktualisiereLagerbestand = CommandEndpointBuilder
			.forCommand(AktualisiereLagerbestand.class).withChannel(SagaConnectionConfig.PRODUKT_SERVICE_CHANNEL)
			.withReply(Success.class).build();

	public final CommandEndpoint<AktualisiereLagerbestandZurueck> aktualisiereLagerbestandZurueck = CommandEndpointBuilder
			.forCommand(AktualisiereLagerbestandZurueck.class).withChannel(SagaConnectionConfig.PRODUKT_SERVICE_CHANNEL)
			.withReply(Success.class).build();
}
