package de.philippjanzen.masterthesis.bestellungservice.saga.repo;

import javax.persistence.CascadeType;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OneToOne;

import org.springframework.beans.factory.annotation.Autowired;

import io.eventuate.tram.sagas.orchestration.SagaInstance;
import io.eventuate.tram.sagas.orchestration.SagaInstanceRepository;
import io.eventuate.tram.sagas.orchestration.SagaInstanceRepositoryJdbc;

public class SagaInstanceRepositoryImpl implements SagaInstanceRepository {

	@Autowired
	private SagaRepository sagaRepository;

	public SagaInstanceRepositoryImpl() {
	}

	@Override
	public SagaInstance find(String sagaType, String sagaId) {
		SagaInstance sagaInstance = sagaRepository.findById(sagaId).get().getSagaInstance();
		if (sagaInstance.getSagaType().equals(sagaType)) {
			return sagaInstance;
		} else {
			throw new EntityNotFoundException();
		}
	}

	@Override
	public void save(SagaInstance sagaInstance) {
		SagaInstanceEntity savedEntity = sagaRepository.save(new SagaInstanceEntity(sagaInstance));
		sagaInstance.setId(savedEntity.getId());
	}

	@Override
	public void update(SagaInstance sagaInstance) {
		sagaRepository.save(new SagaInstanceEntity(sagaInstance));
	}

}
