package de.philippjanzen.masterthesis.bestellungservice.saga.proxies;

import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorb;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorbZurueck;
import io.eventuate.tram.commands.common.Success;
import io.eventuate.tram.sagas.simpledsl.CommandEndpoint;
import io.eventuate.tram.sagas.simpledsl.CommandEndpointBuilder;

public class WarenkorbServiceProxy {
	public final CommandEndpoint<LeereWarenkorb> leereWarenkorb = CommandEndpointBuilder
			.forCommand(LeereWarenkorb.class).withChannel(SagaConnectionConfig.WARENKORB_SERVICE_CHANNEL)
			.withReply(Success.class).build();

	public final CommandEndpoint<LeereWarenkorbZurueck> leereWarenkorbZurueck = CommandEndpointBuilder
			.forCommand(LeereWarenkorbZurueck.class).withChannel(SagaConnectionConfig.WARENKORB_SERVICE_CHANNEL)
			.withReply(Success.class).build();
}
