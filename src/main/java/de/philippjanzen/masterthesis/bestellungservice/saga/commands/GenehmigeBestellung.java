package de.philippjanzen.masterthesis.bestellungservice.saga.commands;

import io.eventuate.tram.commands.common.Command;

public class GenehmigeBestellung implements Command {

	private String bestellungId;
	private String transactionId;
	private String zahlungsreferenz;

	public GenehmigeBestellung() {
	}

	public GenehmigeBestellung(String transactionId, String bestellungId, String zahlungsreferenz) {
		this.transactionId = transactionId;
		this.bestellungId = bestellungId;
		this.zahlungsreferenz = zahlungsreferenz;
	}

	public String getId() {
		return bestellungId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getBestellungId() {
		return bestellungId;
	}

	public void setBestellungId(String bestellungId) {
		this.bestellungId = bestellungId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getZahlungsreferenz() {
		// TODO Auto-generated method stub
		return zahlungsreferenz;
	}

}
