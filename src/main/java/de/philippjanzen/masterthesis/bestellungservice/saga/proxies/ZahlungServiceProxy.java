package de.philippjanzen.masterthesis.bestellungservice.saga.proxies;

import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.TaetigeGutschrift;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.TaetigeZahlung;
import de.philippjanzen.masterthesis.sharedservice.saga.replies.TaetigeZahlungAntwort;
import io.eventuate.tram.commands.common.Success;
import io.eventuate.tram.sagas.simpledsl.CommandEndpoint;
import io.eventuate.tram.sagas.simpledsl.CommandEndpointBuilder;

public class ZahlungServiceProxy {
	public final CommandEndpoint<TaetigeZahlung> taetigeZahlung = CommandEndpointBuilder
			.forCommand(TaetigeZahlung.class).withChannel(SagaConnectionConfig.ZAHLUNG_SERVICE_CHANNEL)
			.withReply(TaetigeZahlungAntwort.class).build();

	public final CommandEndpoint<TaetigeGutschrift> taetigeGutschrift = CommandEndpointBuilder
			.forCommand(TaetigeGutschrift.class).withChannel(SagaConnectionConfig.ZAHLUNG_SERVICE_CHANNEL)
			.withReply(Success.class).build();
}
