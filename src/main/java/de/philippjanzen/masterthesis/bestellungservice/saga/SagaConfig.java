package de.philippjanzen.masterthesis.bestellungservice.saga;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.philippjanzen.masterthesis.bestellungservice.saga.handlers.BestellungCommandHandlers;
import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.BestellungServiceProxy;
import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.ProduktServiceProxy;
import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.WarenkorbServiceProxy;
import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.ZahlungServiceProxy;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;
import de.philippjanzen.masterthesis.sharedservice.saga.channels.ChannelMappingImpl;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.CommandProducerImpl;
import de.philippjanzen.masterthesis.sharedservice.saga.consumers.MessageConsumerImpl;
import de.philippjanzen.masterthesis.sharedservice.saga.producers.MessageProducerImpl;
import io.eventuate.tram.commands.common.ChannelMapping;
import io.eventuate.tram.commands.producer.CommandProducer;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.consumer.MessageConsumer;
import io.eventuate.tram.messaging.producer.MessageProducer;
import io.eventuate.tram.sagas.orchestration.SagaCommandProducer;
import io.eventuate.tram.sagas.orchestration.SagaInstanceRepository;
import io.eventuate.tram.sagas.orchestration.SagaInstanceRepositoryJdbc;
import io.eventuate.tram.sagas.orchestration.SagaManager;
import io.eventuate.tram.sagas.orchestration.SagaManagerImpl;
import io.eventuate.tram.sagas.participant.SagaCommandDispatcher;
import io.eventuate.tram.sagas.participant.SagaLockManager;
import io.eventuate.tram.sagas.participant.SagaLockManagerImpl;

@Configuration
public class SagaConfig {

	@Bean
	public SagaManager<ErstelleBestellungSagaState> erstelleBestellungSagaManager(ErstelleBestellungSaga saga,
			SagaInstanceRepository sagaInstanceRepository, CommandProducer commandProducer,
			MessageConsumer messageConsumer, ChannelMapping channelMapping, SagaLockManager sagaLockmanager,
			SagaCommandProducer sagaCommandProducer) {
		return new SagaManagerImpl<>(saga, sagaInstanceRepository, commandProducer, messageConsumer, channelMapping,
				sagaLockmanager, sagaCommandProducer);
	}

	@Bean
	ErstelleBestellungSaga erstelleBestellungSaga(BestellungServiceProxy bestellungServiceProxy,
			ProduktServiceProxy produktServiceProxy, WarenkorbServiceProxy warenkorbServiceProxy,
			ZahlungServiceProxy zahlungServiceProxy) {
		return new ErstelleBestellungSaga(bestellungServiceProxy, produktServiceProxy, warenkorbServiceProxy,
				zahlungServiceProxy);
	}

	@Bean
	BestellungServiceProxy bestellungServiceProxy() {
		return new BestellungServiceProxy();
	}

	@Bean
	ProduktServiceProxy produktServiceProxy() {
		return new ProduktServiceProxy();
	}

	@Bean
	WarenkorbServiceProxy warenkorbServiceProxy() {
		return new WarenkorbServiceProxy();
	}

	@Bean
	ZahlungServiceProxy zahlungServiceProxy() {
		return new ZahlungServiceProxy();
	}

	@Bean
	SagaInstanceRepository sagaInstanceRepository() {
		return new SagaInstanceRepositoryJdbc();
	}

	@Bean
	CommandProducer commandProducer(MessageProducer messageProducer, ChannelMapping channelMapping) {
		return new CommandProducerImpl(messageProducer, channelMapping);
	}

	@Bean
	SagaCommandProducer sagaCommandProducer(CommandProducer commandProducer) {
		return new SagaCommandProducer(commandProducer);
	}

	@Bean
	MessageProducer messageProducer(RabbitTemplate rabbitTemplate, AmqpConnectionConfig rabbitConnectionConfig) {
		return new MessageProducerImpl(rabbitTemplate, rabbitConnectionConfig);
	}

	@Bean
	SagaLockManager sagaLockManager() {
		return new SagaLockManagerImpl();
	}

	@Bean
	ChannelMapping channelMapping() {
		return new ChannelMappingImpl();
	}

	@Bean
	MessageConsumer messageConsumer() {
		return new MessageConsumerImpl();
	}

	@Bean
	public SagaCommandDispatcher sagaCommandDispatcher(BestellungCommandHandlers bestellungCommandHandlers) {
		return new SagaCommandDispatcher("bestellungService", bestellungCommandHandlers.commandHandlers());
	}

	@Bean
	public BestellungCommandHandlers bestellungCommandHandlers() {
		return new BestellungCommandHandlers();
	}
}
