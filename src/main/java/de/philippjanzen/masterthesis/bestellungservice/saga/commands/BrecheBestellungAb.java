package de.philippjanzen.masterthesis.bestellungservice.saga.commands;

import io.eventuate.tram.commands.common.Command;

public class BrecheBestellungAb implements Command {

	private String bestellungId;
	private String transactionId;

	public BrecheBestellungAb() {
	}

	public BrecheBestellungAb(String transactionId, String bestellungId) {
		this.bestellungId = bestellungId;
		this.transactionId = transactionId;
	}

	public String getId() {
		return this.bestellungId;
	}

	public String getBestellungId() {
		return bestellungId;
	}

	public void setBestellungId(String bestellungId) {
		this.bestellungId = bestellungId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
