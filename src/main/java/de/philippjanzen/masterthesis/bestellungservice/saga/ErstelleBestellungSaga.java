package de.philippjanzen.masterthesis.bestellungservice.saga;

import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.BestellungServiceProxy;
import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.ProduktServiceProxy;
import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.WarenkorbServiceProxy;
import de.philippjanzen.masterthesis.bestellungservice.saga.proxies.ZahlungServiceProxy;
import de.philippjanzen.masterthesis.sharedservice.saga.replies.TaetigeZahlungAntwort;
import io.eventuate.tram.sagas.orchestration.SagaDefinition;
import io.eventuate.tram.sagas.simpledsl.SimpleSaga;

public class ErstelleBestellungSaga implements SimpleSaga<ErstelleBestellungSagaState> {

	private SagaDefinition<ErstelleBestellungSagaState> sagaDefinition;

	public ErstelleBestellungSaga(BestellungServiceProxy bestellungService, ProduktServiceProxy produktService,
			WarenkorbServiceProxy warenkorbService, ZahlungServiceProxy zahlungService) {
		this.sagaDefinition = step()
				.withCompensation(bestellungService.abbrechen,
						ErstelleBestellungSagaState::erzeugeBrecheBestellungAbCommand)
				.step()
				.invokeParticipant(zahlungService.taetigeZahlung,
						ErstelleBestellungSagaState::erzeugeTaetigeZahlungCommand)
				.onReply(TaetigeZahlungAntwort.class, ErstelleBestellungSagaState::handleTaetigeZahlungAntwort)
				.withCompensation(zahlungService.taetigeGutschrift,
						ErstelleBestellungSagaState::erzeugeTaetigeGutschriftCommand)
				.step()
				.invokeParticipant(warenkorbService.leereWarenkorb,
						ErstelleBestellungSagaState::erzeugeLeereWarenkorbCommand)
				.withCompensation(warenkorbService.leereWarenkorbZurueck,
						ErstelleBestellungSagaState::erzeugeLeereWarenkorbZurueckCommand)
				.step()
				.invokeParticipant(produktService.aktualisiereLagerbestand,
						ErstelleBestellungSagaState::erzeugeAktualisiereLagerbestandCommand)
				.withCompensation(produktService.aktualisiereLagerbestandZurueck,
						ErstelleBestellungSagaState::erzeugeAktualisiereLagerbestandZurueckCommand)
				.step().invokeParticipant(bestellungService.genehmigeBestellung,
						ErstelleBestellungSagaState::erzeugeGenehmigeBestellungCommand)
				.build();
	}

	public SagaDefinition<ErstelleBestellungSagaState> getSagaDefinition() {
		return sagaDefinition;
	}

	@Override
	public void onSagaCompletedSuccessfully(String sagaId, ErstelleBestellungSagaState data) {
		// TODO Auto-generated method stub
		SimpleSaga.super.onSagaCompletedSuccessfully(sagaId, data);
	}

	@Override
	public void onSagaRolledBack(String sagaId, ErstelleBestellungSagaState data) {
		// TODO Auto-generated method stub
		SimpleSaga.super.onSagaRolledBack(sagaId, data);
	}

}
