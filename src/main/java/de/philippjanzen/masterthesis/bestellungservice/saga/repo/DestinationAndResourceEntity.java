package de.philippjanzen.masterthesis.bestellungservice.saga.repo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DestinationAndResourceEntity {

	@Id()
	@GeneratedValue
	private long id;
	private String destination;
	private String resource;

	public DestinationAndResourceEntity() {
	}

	public DestinationAndResourceEntity(String destination, String resource) {
		this.destination = destination;
		this.resource = resource;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

}
