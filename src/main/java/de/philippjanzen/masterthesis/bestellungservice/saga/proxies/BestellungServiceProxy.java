package de.philippjanzen.masterthesis.bestellungservice.saga.proxies;

import de.philippjanzen.masterthesis.bestellungservice.saga.commands.BrecheBestellungAb;
import de.philippjanzen.masterthesis.bestellungservice.saga.commands.GenehmigeBestellung;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import io.eventuate.tram.commands.common.Success;
import io.eventuate.tram.sagas.simpledsl.CommandEndpoint;
import io.eventuate.tram.sagas.simpledsl.CommandEndpointBuilder;

public class BestellungServiceProxy {

	public final CommandEndpoint<GenehmigeBestellung> genehmigeBestellung = CommandEndpointBuilder
			.forCommand(GenehmigeBestellung.class).withChannel(SagaConnectionConfig.BESTELLUNG_SERVICE_CHANNEL)
			.withReply(Success.class).build();

	public final CommandEndpoint<BrecheBestellungAb> abbrechen = CommandEndpointBuilder
			.forCommand(BrecheBestellungAb.class).withChannel(SagaConnectionConfig.BESTELLUNG_SERVICE_CHANNEL)
			.withReply(Success.class).build();
}
