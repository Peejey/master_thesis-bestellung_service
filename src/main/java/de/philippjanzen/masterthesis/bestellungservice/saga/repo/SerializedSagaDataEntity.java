package de.philippjanzen.masterthesis.bestellungservice.saga.repo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung;

@Entity
public class SerializedSagaDataEntity {

	@Id()
	@GeneratedValue
	private long id;
	private String sagaDataType;
	private String sagaDataJSON;

	@OneToOne
	@PrimaryKeyJoinColumn
	private SagaInstanceEntity sagaInstanceEntity;

	public SerializedSagaDataEntity() {
	}

	public SerializedSagaDataEntity(String sagaDataType, String sagaDataJSON) {
		super();
		this.sagaDataType = sagaDataType;
		this.sagaDataJSON = sagaDataJSON;
	}

	public String getSagaDataType() {
		return sagaDataType;
	}

	public void setSagaDataType(String sagaDataType) {
		this.sagaDataType = sagaDataType;
	}

	public String getSagaDataJSON() {
		return sagaDataJSON;
	}

	public void setSagaDataJSON(String sagaDataJSON) {
		this.sagaDataJSON = sagaDataJSON;
	}

}
