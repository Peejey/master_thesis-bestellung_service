package de.philippjanzen.masterthesis.bestellungservice.saga;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import de.philippjanzen.masterthesis.bestellungservice.saga.commands.BrecheBestellungAb;
import de.philippjanzen.masterthesis.bestellungservice.saga.commands.GenehmigeBestellung;
import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestand;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestandZurueck;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorb;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorbZurueck;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.TaetigeGutschrift;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.TaetigeZahlung;
import de.philippjanzen.masterthesis.sharedservice.saga.replies.TaetigeZahlungAntwort;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;

public class ErstelleBestellungSagaState {

	private String transactionId;
	private String bestellungId;
	private String warenkorbId;
	private List<LagerbestandsAktualisierungDTO> lagerbestaende;
	private ZahlungsinformationDTO zahlung;
	private String zahlungId;

	public ErstelleBestellungSagaState() {
	}

	public ErstelleBestellungSagaState(String transactionId, String bestellungId, String warenkorbId,
			List<LagerbestandsAktualisierungDTO> lagerbestaende, ZahlungsinformationDTO zahlung) {
		this.transactionId = transactionId;
		this.bestellungId = bestellungId;
		this.warenkorbId = warenkorbId;
		this.lagerbestaende = lagerbestaende;
		this.zahlung = zahlung;
	}

	GenehmigeBestellung erzeugeGenehmigeBestellungCommand() {
		return new GenehmigeBestellung(transactionId, bestellungId, zahlungId);
	}

	BrecheBestellungAb erzeugeBrecheBestellungAbCommand() {
		return new BrecheBestellungAb(transactionId, bestellungId);
	}

	AktualisiereLagerbestand erzeugeAktualisiereLagerbestandCommand() {
		return new AktualisiereLagerbestand(transactionId, lagerbestaende);
	}

	AktualisiereLagerbestandZurueck erzeugeAktualisiereLagerbestandZurueckCommand() {
		return new AktualisiereLagerbestandZurueck(transactionId, lagerbestaende);
	}

	LeereWarenkorb erzeugeLeereWarenkorbCommand() {
		return new LeereWarenkorb(transactionId, warenkorbId);
	}

	LeereWarenkorbZurueck erzeugeLeereWarenkorbZurueckCommand() {
		return new LeereWarenkorbZurueck(transactionId, warenkorbId);
	}

	TaetigeZahlung erzeugeTaetigeZahlungCommand() {
		return new TaetigeZahlung(transactionId, zahlung);
	}

	TaetigeGutschrift erzeugeTaetigeGutschriftCommand() {
		return new TaetigeGutschrift(transactionId, zahlungId);
	}

	void handleTaetigeZahlungAntwort(TaetigeZahlungAntwort taetigeZahlungAntwort) {
		this.zahlungId = taetigeZahlungAntwort.getZahlungId();
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getBestellungId() {
		return bestellungId;
	}

	public void setBestellungId(String bestellungId) {
		this.bestellungId = bestellungId;
	}

	public String getWarenkorbId() {
		return warenkorbId;
	}

	public void setWarenkorbId(String warenkorbId) {
		this.warenkorbId = warenkorbId;
	}

	public List<LagerbestandsAktualisierungDTO> getLagerbestaende() {
		return lagerbestaende;
	}

	public void setLagerbestaende(List<LagerbestandsAktualisierungDTO> lagerbestaende) {
		this.lagerbestaende = lagerbestaende;
	}

	public ZahlungsinformationDTO getZahlung() {
		return zahlung;
	}

	public void setZahlung(ZahlungsinformationDTO zahlung) {
		this.zahlung = zahlung;
	}

	public String getZahlungId() {
		return zahlungId;
	}

	public void setZahlungId(String zahlungId) {
		this.zahlungId = zahlungId;
	}

}
