package de.philippjanzen.masterthesis.bestellungservice.saga.repo;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import org.springframework.util.SerializationUtils;

import io.eventuate.tram.sagas.orchestration.DestinationAndResource;
import io.eventuate.tram.sagas.orchestration.SagaInstance;
import io.eventuate.tram.sagas.orchestration.SagaInstanceRepositoryJdbc;
import io.eventuate.tram.sagas.orchestration.SerializedSagaData;

@Entity
public class SagaInstanceEntity {

	@Id
	private String sagaId;
	private String sagaType;
	private String stateName;
	private String lastRequestId;
	@OneToOne(cascade = CascadeType.ALL)
	private SerializedSagaDataEntity serializedSagaData;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "sagaInstanceEntity_sagaId")
	private Set<DestinationAndResourceEntity> destinationsAndResources = new HashSet<>();

	public SagaInstanceEntity(SagaInstance sagaInstance) {
		sagaId = sagaInstance.getId();
		sagaType = sagaInstance.getSagaType();
		stateName = sagaInstance.getStateName();
		lastRequestId = sagaInstance.getLastRequestId();
		serializedSagaData = new SerializedSagaDataEntity(sagaInstance.getSerializedSagaData().getSagaDataType(),
				sagaInstance.getSerializedSagaData().getSagaDataJSON());
		sagaInstance.getDestinationsAndResources().forEach(x -> {
			destinationsAndResources.add(new DestinationAndResourceEntity(x.getDestination(), x.getResource()));
		});

	}

	public SagaInstance getSagaInstance() {
		Set<DestinationAndResource> toConvert = new HashSet<>();
		destinationsAndResources.forEach(x -> {
			toConvert.add(new DestinationAndResource(x.getDestination(), x.getResource()));
		});
		SagaInstance sagaInstance = new SagaInstance(sagaType, sagaId, stateName, lastRequestId,
				new SerializedSagaData(serializedSagaData.getSagaDataType(), serializedSagaData.getSagaDataJSON()),
				toConvert);
		return sagaInstance;

	}

	public String getId() {
		return sagaId;
	}

	public void setId(String id) {
		this.sagaId = id;
	}

	public String getSagaType() {
		return sagaType;
	}

	public void setSagaType(String sagaType) {
		this.sagaType = sagaType;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getLastRequestId() {
		return lastRequestId;
	}

	public void setLastRequestId(String lastRequestId) {
		this.lastRequestId = lastRequestId;
	}

	public SerializedSagaDataEntity getSerializedSagaData() {
		return serializedSagaData;
	}

	public void setSerializedSagaData(SerializedSagaDataEntity serializedSagaData) {
		this.serializedSagaData = serializedSagaData;
	}

	public Set<DestinationAndResourceEntity> getDestinationsAndResources() {
		return destinationsAndResources;
	}

	public void setDestinationsAndResources(Set<DestinationAndResourceEntity> destinationsAndResources) {
		this.destinationsAndResources = destinationsAndResources;
	}

	@PrePersist
	private void generateId() {
		if (sagaId == null || sagaId.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

}
