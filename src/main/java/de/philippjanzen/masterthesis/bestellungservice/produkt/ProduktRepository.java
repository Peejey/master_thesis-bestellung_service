package de.philippjanzen.masterthesis.bestellungservice.produkt;

import org.springframework.data.repository.CrudRepository;

public interface ProduktRepository extends CrudRepository<Produkt, String> {

}
