package de.philippjanzen.masterthesis.bestellungservice.produkt;

import org.springframework.beans.factory.annotation.Autowired;

import de.philippjanzen.masterthesis.bestellungservice.bestellung.ArtikelInformationen;
import de.philippjanzen.masterthesis.bestellungservice.entities.Artikel;
import de.philippjanzen.masterthesis.bestellungservice.mapper.ProduktMapper;

public class ProduktInformationenAdapter implements ArtikelInformationen {

	@Autowired
	private ProduktMapper produktMapper;

	@Override
	public void gleicheInformationenAb(Artikel artikel) {
		produktMapper.gleicheArtikelAb(artikel);
	}

}
