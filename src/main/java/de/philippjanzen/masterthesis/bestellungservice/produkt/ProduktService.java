package de.philippjanzen.masterthesis.bestellungservice.produkt;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.bestellungservice.entities.Artikel;
import de.philippjanzen.masterthesis.bestellungservice.rest.ProduktAPI;

@Service
public class ProduktService {

	private ProduktRepository produktRepository;
	private ProduktAPI produktApi;

	public ProduktService(ProduktRepository produktRepository, ProduktAPI restApi) {
		this.produktRepository = produktRepository;
		this.produktApi = restApi;
	}

	public Produkt findeProduktAnhandArtikel(Artikel artikel) {
		Optional<Produkt> optProdukt = produktRepository.findById(artikel.getReferenzId());
		if (optProdukt.isPresent()) {
			return optProdukt.get();
		} else {
			List<Produkt> produkte = produktApi.leseProdukte();
			try {
				produktRepository.saveAll(produktApi.leseProdukte());
			} catch (Exception z) {
				// temp ignore
			}
			return produkte.stream().filter(p -> p.getId().equals(artikel.getReferenzId())).findFirst().get();
		}
	}

	public void aktualisiereProdukte() {
		try {
			produktRepository.saveAll(produktApi.leseProdukte());
		} catch (Exception z) {
			// temp ignore
		}
	}
}
