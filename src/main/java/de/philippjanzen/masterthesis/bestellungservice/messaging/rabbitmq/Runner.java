package de.philippjanzen.masterthesis.bestellungservice.messaging.rabbitmq;

import io.eventuate.javaclient.commonimpl.JSonMapper;
import io.eventuate.tram.commands.common.CommandMessageHeaders;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.producer.MessageBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.SerializationUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;

//@Component
//public class Runner implements CommandLineRunner {
//
//	private final RabbitTemplate rabbitTemplate;
//	private final Receiver receiver;
//
//	public Runner(Receiver receiver, RabbitTemplate rabbitTemplate) {
//		this.receiver = receiver;
//		this.rabbitTemplate = rabbitTemplate;
//	}
//
//	@Override
//	public void run(String... args) throws Exception {
//		System.out.println("Sending message...");
//		SagaCustomMessage message = new SagaCustomMessage(MessageBuilder.withPayload("Hallo")
//				.withHeader(CommandMessageHeaders.DESTINATION, ServiceChannels.PRODUKT_SERVICE_CHANNEL)
//				.withHeader(Message.ID, UUID.randomUUID().toString()).build());
//		rabbitTemplate.convertAndSend(MessageConfig.MICROSERVICES_TOPIC_EXCHANGE, "microservices.produktservice.baz",
//				message);
//
//	}
//
//}