package de.philippjanzen.masterthesis.bestellungservice.messaging.rabbitmq;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfigImpl;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.Receiver;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;

@Configuration
public class MessageConfig {

	@Bean
	public AmqpConnectionConfig rabbitmqConnectionConfig() {
		return new AmqpConnectionConfigImpl();
	}

	@Bean
	public ConnectionFactory connectionFactory(AmqpConnectionConfig rabbitmqConnectionConfig) {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory(
				rabbitmqConnectionConfig.getHostname());
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}

	@Bean
	public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory,
			Jackson2JsonMessageConverter jackson2JsonMessageConverter) {
		final var rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(jackson2JsonMessageConverter);
		return rabbitTemplate;
	}

	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter(ObjectMapper objectMapper) {
		return new Jackson2JsonMessageConverter(objectMapper);
	}

	@Bean
	Declarables topicExchanges() {
		return new Declarables(sagaExchange(), eventsExchange());
	}

	@Bean
	Declarables queues() {
		return new Declarables(bestellungSagaQueue(), bestellungEventsQueue(), produktEventsQueue(),
				warenkorbEventsQueue(), zahlungEventsQueue());
	}

	@Bean
	Declarables bindings(AmqpConnectionConfig rabbitmqConnectionConfig) {
		return new Declarables(
				BindingBuilder.bind(bestellungEventsQueue()).to(eventsExchange())
						.with(rabbitmqConnectionConfig.getRoute(EventsConnectionConfig.ZAHLUNG_SERVICE_CHANNEL)),
				BindingBuilder.bind(bestellungEventsQueue()).to(eventsExchange())
						.with(rabbitmqConnectionConfig.getRoute(EventsConnectionConfig.WARENKORB_SERVICE_CHANNEL)),
				BindingBuilder.bind(bestellungEventsQueue()).to(eventsExchange())
						.with(rabbitmqConnectionConfig.getRoute(EventsConnectionConfig.PRODUKT_SERVICE_CHANNEL)),
				BindingBuilder.bind(bestellungSagaQueue()).to(sagaExchange()).with(
						rabbitmqConnectionConfig.getRoute(SagaConnectionConfig.BESTELLUNG_SERVICE_CHANNEL) + ".#"));
	}

	@Bean
	ChannelManager<DomainEventEnvelope> eventsChannelManager() {
		return new ChannelManager<>();
	}

	@Bean
	ChannelManager<SagaCustomMessage> sagaChannelManager() {
		return new ChannelManager<>();
	}

	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
			MessageListenerAdapter listenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(SagaConnectionConfig.BESTELLUNG_SERVICE_CHANNEL,
				EventsConnectionConfig.ZAHLUNG_SERVICE_CHANNEL);
		container.setMessageListener(listenerAdapter);
		container.setDefaultRequeueRejected(false);
		container.setFailedDeclarationRetryInterval(10000);
		return container;
	}

	@Bean
	MessageListenerAdapter listenerAdapter(Receiver receiver,
			Jackson2JsonMessageConverter jackson2JsonMessageConverter) {
		MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(receiver, "receiveMessage");
		messageListenerAdapter.setMessageConverter(jackson2JsonMessageConverter);
		return messageListenerAdapter;
	}

	@Bean
	Receiver receiver() {
		return new Receiver(sagaChannelManager(), eventsChannelManager());
	}

	TopicExchange eventsExchange() {
		return new TopicExchange(rabbitmqConnectionConfig().getEventsExchange());
	}

	TopicExchange sagaExchange() {
		return new TopicExchange(rabbitmqConnectionConfig().getSagaExchange());
	}

	Queue bestellungEventsQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "microservices-events-dead-letter-exchange");
		return new Queue(EventsConnectionConfig.BESTELLUNG_SERVICE_CHANNEL, false, false, false, args);
	}

	Queue zahlungEventsQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "microservices-events-dead-letter-exchange");
		return new Queue(EventsConnectionConfig.ZAHLUNG_SERVICE_CHANNEL, false, false, false, args);
	}

	Queue warenkorbEventsQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "microservices-events-dead-letter-exchange");
		return new Queue(EventsConnectionConfig.WARENKORB_SERVICE_CHANNEL, false, false, false, args);
	}

	Queue produktEventsQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "microservices-events-dead-letter-exchange");
		return new Queue(EventsConnectionConfig.PRODUKT_SERVICE_CHANNEL, false, false, false, args);
	}

	Queue bestellungSagaQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "microservices-saga-dead-letter-exchange");
		return new Queue(SagaConnectionConfig.BESTELLUNG_SERVICE_CHANNEL, false, false, false, args);
	}

}
